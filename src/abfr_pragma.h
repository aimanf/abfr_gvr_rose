#ifndef __ABFR_PRAGMA_H__
#define __ABFR_PRAGMA_H__
/**
 * ABFR pragma support from ROSE
 * 
 * author Aiman Fang
 * aimanf@uchicago.edu
 * 
 */

#include "rose.h"
#include <iostream>

using namespace SageInterface;
using namespace SageBuilder;

enum abfr_pragma_enum {
    pragma_abfr_gvr_init,
    pragma_abfr_init_version,
    pragma_abfr_versioning,
    pragma_abfr_recover,
    pragma_abfr_gvr_finalize
};

class ABFR_PragmaAttribute: public AstAttribute 
{
    public:
        SgNode * node; 
        enum abfr_pragma_enum pragma_type;

        ABFR_PragmaAttribute(SgNode* n , abfr_pragma_enum p_type): node(n), pragma_type(p_type) { }

        virtual std::string toString ()
        {
            std::string result;
            result += "#pragma ";
            switch (pragma_type)
            {
                case pragma_abfr_gvr_init:
                    result += "abfr_gvr_init";
                    break;
                case pragma_abfr_init_version:
                    result += "abfr_init_version";
                    break;
                case pragma_abfr_versioning:
                    result += "abfr_versioning";
                    break;
                case pragma_abfr_recover:
                    result += "abfr_recover";
                    break;
                case pragma_abfr_gvr_finalize:
                    result += "abfr_gvr_finalize";
                    break;
                default:
                    std::cerr<<"Error. ABFR_PragmaAttribute::toString(), illegal pragma type."<<std::endl;
                    assert(false);
            }  
            return result; 
        }

}; 

class ABFR_InitGVR_PragmaAttribute: public ABFR_PragmaAttribute
{
    public:
        SgVarRefExp *argc;
        SgVarRefExp *argv;

        ABFR_InitGVR_PragmaAttribute(SgNode *n , abfr_pragma_enum p_type, 
            SgVarRefExp *c, SgVarRefExp *v): 
                ABFR_PragmaAttribute(n, p_type), argc(c), argv(v) { }

        virtual std::string toString()
        {
            std::string result = ABFR_PragmaAttribute::toString();
            if (argc != NULL)
                result += " argc="+ argc->unparseToString();
            if (argv != NULL)
                result += " argv="+ argv->unparseToString();
            return result;
        }
};

class ABFR_FinalizeGVR_PragmaAttribute: public ABFR_PragmaAttribute
{
    public:
        ABFR_FinalizeGVR_PragmaAttribute(SgNode *n , abfr_pragma_enum p_type): 
                ABFR_PragmaAttribute(n, p_type) { }

        virtual std::string toString()
        {
            std::string result = ABFR_PragmaAttribute::toString();
                result += " finalize.";
            return result;
        }
};

class ABFR_InitVersion_PragmaAttribute: public ABFR_PragmaAttribute
{
    public:
        SgVarRefExp *data;
        SgVarRefExp *ndims;
        SgVarRefExp *count;
        SgType *type;
        SgStringVal *gds;

        ABFR_InitVersion_PragmaAttribute(SgNode *n , abfr_pragma_enum p_type, 
            SgVarRefExp *d, SgVarRefExp *dim, SgVarRefExp *c, SgType *t, SgStringVal *g): 
                ABFR_PragmaAttribute(n, p_type), data(d), ndims(dim), count(c),
                type(t), gds(g) { }

        virtual std::string toString()
        {
            std::string result = ABFR_PragmaAttribute::toString();
            if (data != NULL)
                result += " data="+ data->unparseToString();
            if (ndims != NULL)
                result += " ndims="+ ndims->unparseToString();
            if (count != NULL)
                result += " count="+ count->unparseToString();
            if (type != NULL)
                result += " type="+ type->class_name();
            if (gds != NULL)
                result += " gds="+ gds->get_value();
            return result;
        }
};

class ABFR_Versioning_PragmaAttribute: public ABFR_PragmaAttribute
{
    public:
        SgVarRefExp *data;
        SgVarRefExp *ld;
        SgVarRefExp *lo_index;
        SgVarRefExp *hi_index;
        SgStringVal *gds;

        ABFR_Versioning_PragmaAttribute(SgNode *n , abfr_pragma_enum p_type, 
            SgVarRefExp *d_name, SgVarRefExp *l, SgVarRefExp *lo, SgVarRefExp *hi, 
            SgStringVal *g): 
                ABFR_PragmaAttribute(n, p_type), data(d_name), ld(l), lo_index(lo), 
                hi_index(hi), gds(g) { }

        virtual std::string toString()
        {
            std::string result = ABFR_PragmaAttribute::toString();
            if (data != NULL)
                result += " data=" + data->unparseToString();
            if (ld != NULL)
                result += " ld=" + ld->unparseToString();
            if (lo_index != NULL)
                result += " lo_index=" + lo_index->unparseToString();
            if (hi_index != NULL)
                result += " high_index=" + hi_index->unparseToString();
            if (gds != NULL)
                result += " gds="+ gds->get_value();
            return result;
        }
};

class ABFR_Recover_PragmaAttribute: public ABFR_PragmaAttribute
{
    public:
        SgVarRefExp * data;
        SgStringVal * gds;
        SgVarRefExp * error_index;
        SgVarRefExp * step;
        SgVarRefExp * latency;
        SgVarRefExp * interval;
        SgVarRefExp * error_tol;
        SgFunctionRefExp * invert_func;
        SgFunctionRefExp * compute_func;

        ABFR_Recover_PragmaAttribute(SgNode * n, abfr_pragma_enum p_type, 
            SgVarRefExp * d_name, SgStringVal * g, SgVarRefExp * error, 
            SgVarRefExp * stp, SgVarRefExp * l, SgVarRefExp * i, SgVarRefExp * tol,
            SgFunctionRefExp * inverse, SgFunctionRefExp * compute): 
                ABFR_PragmaAttribute(n, p_type), data(d_name), gds(g), error_index(error), 
                step(stp), latency(l), interval(i), error_tol(tol), invert_func(inverse), 
                compute_func(compute) {}

        virtual std::string toString()
        {
            std::string result = ABFR_PragmaAttribute::toString();
            if (data != NULL)
                result += " data="+ data->unparseToString();
            if (gds != NULL)
                result += " gds="+ gds->get_value();
            if (error_index != NULL)
                result += " error_index="+ error_index->unparseToString();
            if (step != NULL)
                result += " step="+ step->unparseToString();
            if (latency != NULL)
                result += " latency="+ latency->unparseToString();
            if (interval != NULL)
                result += " interval="+ interval->unparseToString();
            if (error_tol != NULL)
                result += " error_tol="+ error_tol->unparseToString();
            if (invert_func != NULL) {
                SgFunctionSymbol *inverse = invert_func->get_symbol();
                result += " invert_func="+ inverse->get_name();
            }
            if (compute_func != NULL) {
                SgFunctionSymbol * compute = compute_func->get_symbol();
                result += " compute_func="+ compute->get_name();
            }
            return result;
        }

};

extern AstAttribute* parse_ABFR_Pragma(SgPragmaDeclaration* pragmaDecl);

#endif /* __ABFR_PRAGMA_H__ */
