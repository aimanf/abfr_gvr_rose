Author: Aiman Fang

University of Chicago

aimanf@uchicago.edu

This project utilizes rose compiler to implement a translator that 
automatically inserts ABFR (application-based focused recovery) functions to a
given source code.  The program augmented with ABFR can efficiently recovery
from latent errors.

