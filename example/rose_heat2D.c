/****************************************************************************
 * FILE: mpi_heat2D.c
 * DESCRIPTIONS:  
 *   HEAT2D Example - Parallelized C Version
 *   This example is based on a simplified two-dimensional heat 
 *   equation domain decomposition.  The initial temperature is computed to be 
 *   high in the middle of the domain and zero at the boundaries.  The 
 *   boundaries are held at zero throughout the simulation.  During the 
 *   time-stepping, an array containing two domains is used; these domains 
 *   alternate between old data and new data.
 *
 * AUTHOR: Aiman Fang - adpated from Blaise Barney and D. Turner. 
 * LAST REVISED: 11/25/17
 ****************************************************************************/
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NONE        -1                  /* indicates no neighbor */
#define DONE        4                  /* message tag */
#define MASTER      0                  /* taskid of first process */
#define LTAG        1                  /* message tag */
#define RTAG        2                  /* message tag */
#include "gds.h" 
int NX;
int NY;
int STEPS;
int taskid;
int numtasks;
int latency;
int interval;
double error_tol = 0.001;
int DetectedStep;
// actual latency
int AL;
/* shadow array for grid */
double **ut;
struct Parms {
double cx;
double cy;}parms = {(0.1), (0.1)};

int main(int argc,char *argv[])
{
  void abfr_recovery(double **u,GDS_gds_t gds_u,int ndims,size_t *count,size_t *ld,size_t *lo_index,size_t *hi_index,int step,int *error_index,int latency,int interval,double error_tol);
  void inidat();
  void prtdat();
  void update();
  int **invert_propagation(int *x,int step,int *nPRC,int *ABFR_PRC_RANK);
/* array for grid */
  double **u;
/* local rows, columns */
  int rows;
  int cols;
  int dest
/* to - from for message send-receive */
;
  int source;
  int left
/* neighbor tasks */
;
  int right;
  int msgtype
/* for message types */
;
  int start
/* misc */
;
  int end;
  int i
/* loop variables */
;
  int ix;
  int iy;
  int it;
  MPI_Status status[4];
  MPI_Request rqst[4];
  double tbegin;
  double tend;
  double telapsed;
  double tb_rcv;
  double te_rcv;
  int error_detected = 0;
  
#pragma abfr_gvr_init argc argv
  GDS_thread_support_t provd_support;
  GDS_init(&argc,&argv,GDS_THREAD_MULTIPLE,&provd_support);
  MPI_Init(&argc,&argv);
  MPI_Comm_size(((MPI_Comm )0x44000000),&numtasks);
  MPI_Comm_rank(((MPI_Comm )0x44000000),&taskid);
// parse parameters
  if (argc < 7) {
    if (taskid == 0) 
      printf("Miss --NX, --NY\n");
    return 0;
  }
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i],"--NX")) {
      NX = atoi(argv[++i]);
      if (taskid == 0) 
        printf("NX=%d\n",NX);
    }
     else if (!strcmp(argv[i],"--NY")) {
      NY = atoi(argv[++i]);
      if (taskid == 0) 
        printf("NY=%d\n",NY);
    }
     else if (!strcmp(argv[i],"--STEPS")) {
      STEPS = atoi(argv[++i]);
      if (taskid == 0) 
        printf("STEPS=%d\n",STEPS);
    }
     else if (!strcmp(argv[i],"--DetectedStep")) {
      DetectedStep = atoi(argv[++i]);
      if (taskid == 0) 
        printf("DetectedStep=%d\n",DetectedStep);
    }
     else if (!strcmp(argv[i],"--AL")) {
      AL = atoi(argv[++i]);
      if (taskid == 0) 
        printf("AL=%d\n",AL);
    }
     else if (!strcmp(argv[i],"--latency")) {
      latency = atoi(argv[++i]);
      if (taskid == 0) 
        printf("latency=%d\n",latency);
    }
     else if (!strcmp(argv[i],"--interval")) {
      interval = atoi(argv[++i]);
      if (taskid == 0) 
        printf("interval=%d\n",interval);
    }
  }
  int ndims = 2;
  size_t count[2] = {NX, NY};
  
#pragma abfr_init_version u ndims count double gds_u
// 2 additional halo rows 
  int abfr_i;
  GDS_size_t *min_chunks;
  min_chunks = ((GDS_size_t *)(calloc(sizeof(GDS_size_t ),ndims)));
  for (abfr_i = 0; abfr_i < ndims; ++abfr_i) 
    min_chunks[abfr_i] = 0;
  GDS_gds_t gds_u;
  MPI_Info abfr_info;
  MPI_Info_create(&abfr_info);
  GDS_datatype_t abfr_type = GDS_DATA_DBL;
  GDS_alloc(ndims,count,min_chunks,abfr_type,GDS_PRIORITY_HIGH,GDS_COMM_WORLD,abfr_info,&gds_u);
  rows = NX / numtasks + 2;
  cols = NY;
  u = ((double **)(calloc(sizeof(double *),rows)));
  ut = ((double **)(calloc(sizeof(double *),rows)));
  for (i = 0; i < rows; i++) {
    u[i] = ((double *)(calloc(sizeof(double ),NY)));
    ut[i] = ((double *)(calloc(sizeof(double ),NY)));
  }
  if (taskid == 0) {
    printf("Starting mpi_heat2D with %d worker tasks.\n",numtasks);
/* Initialize grid */
    printf("Grid size: X= %d  Y= %d  Time steps= %d\n",NX,NY,STEPS);
    printf("Initializing grid and writing initial.dat file...\n");
    prtdat(rows,cols,u,"initial.dat");
/* End of master code */
  }
// initial condition: high in the middle of the domain and zero at the
// boundaries
  inidat(rows,cols,u);
// Assign neighbors
  if (taskid == 0) 
    left = - 1;
   else 
    left = taskid - 1;
  if (taskid == numtasks - 1) 
    right = - 1;
   else 
    right = taskid + 1;
/* Initialize everything - including the borders - to zero */
  for (ix = 0; ix < rows; ix++) {
    for (iy = 0; iy < cols; iy++) {
      ut[ix][iy] = 0.0;
    }
  }
/* Begin doing STEPS iterations.  Must communicate border rows with */
/* neighbors.  If I have the first or last grid row, then I only need */
/* to communicate with one neighbor  */
  MPI_Barrier(((MPI_Comm )0x44000000));
  if (taskid == 0) 
    printf("Begin time steps...\n");
  for (it = 1; it <= STEPS; it++) {
    tbegin = MPI_Wtime();
// exchange left ghost row
    if (left != - 1) {
      MPI_Isend((&u[1][0]),NY,((MPI_Datatype )0x4c00080b),left,2,((MPI_Comm )0x44000000),&rqst[0]);
      MPI_Irecv((&u[0][0]),NY,((MPI_Datatype )0x4c00080b),left,1,((MPI_Comm )0x44000000),&rqst[1]);
    }
// exchange right ghost row
    if (right != - 1) {
      MPI_Isend((&u[rows - 2][0]),NY,((MPI_Datatype )0x4c00080b),right,1,((MPI_Comm )0x44000000),&rqst[2]);
      MPI_Irecv((&u[rows - 1][0]),NY,((MPI_Datatype )0x4c00080b),right,2,((MPI_Comm )0x44000000),&rqst[3]);
    }
    if (left == - 1) {
      MPI_Waitall(2,&rqst[2],&status[2]);
    }
     else if (right == - 1) {
      MPI_Waitall(2,&rqst[0],&status[0]);
    }
     else {
      MPI_Waitall(4,rqst,status);
    }
/* Now call update to update the value of grid points */
    update(rows,cols,u,ut);
    if (error_detected == 0 && it == DetectedStep) {
      MPI_Barrier(((MPI_Comm )0x44000000));
      if (taskid == 0) 
        printf("An error is raised\n");
      int ndims = 2;
      size_t count[2] = {NX, NY};
      size_t ld[1] = {NY};
      size_t lo_index[2] = {(NX / numtasks * taskid), (0)};
      size_t hi_index[2] = {(NX / numtasks * (taskid + 1) - 1), (NY - 1)};
      int error_index[2] = {NX / 2, NY / 2};
      tb_rcv = MPI_Wtime();
      
#pragma abfr_recover u gds_u error_index it latency interval error_tol invert_propagation compute
      abfr_recovery(u,gds_u,ndims,count,ld,lo_index,hi_index,it,error_index,latency,interval,error_tol);
      te_rcv = MPI_Wtime();
      error_detected = 1;
      MPI_Barrier(((MPI_Comm )0x44000000));
    }
    size_t ld[1] = {NY};
    size_t lo_index[2] = {(NX / numtasks * taskid), (0)};
    size_t hi_index[2] = {(NX / numtasks * (taskid + 1) - 1), (NY - 1)};
    if (it % interval == 0) {
      
#pragma abfr_versioning u ld lo_index hi_index gds_u
      GDS_put(u,ld,lo_index,hi_index,gds_u);
      GDS_version_inc(gds_u,1,'\0',0);
      if (taskid == 0) 
        printf("versioning at step %d\n",it);
    }
    tend = MPI_Wtime();
    if (taskid == 0) 
      printf("step %d takes %f seconds\n",it,tend - tbegin);
    telapsed += tend - tbegin;
  }
  if (taskid == 0) {
    printf("Writing final.dat file.\n");
    prtdat(rows,cols,u,"final.dat");
    printf("Simulation time = %f\n",telapsed);
  }
  
#pragma abfr_gvr_finalize
  GDS_free(&gds_u);
  GDS_finalize();
  MPI_Finalize();
}
/**************************************************************************
 *  subroutine update
 ****************************************************************************/

void update(int rows,int cols,double **u,double **ut)
{
  int ix;
  int iy;
  for (ix = 1; ix < rows - 1; ix++) {
    for (iy = 0; iy < cols; iy++) {
      ut[ix][iy] = u[ix][iy] + parms . cx * (u[ix + 1][iy] + u[ix - 1][iy] - 2.0 * u[ix][iy]) + parms . cy * (u[ix][iy + 1] + u[ix][iy - 1] - 2.0 * u[ix][iy]);
    }
  }
  for (ix = 1; ix < rows - 1; ix++) {
    for (iy = 0; iy < cols; iy++) {
      u[ix][iy] = ut[ix][iy];
    }
  }
}
/*****************************************************************************
 *  subroutine inidat
 *****************************************************************************/

void inidat(int rows,int cols,double **u)
{
  int ix;
  int iy;
// actual num of rows assigned to each task excluding 2 halo rows
  int nrows = rows - 2;
  for (ix = 1; ix <= nrows; ix++) {
    for (iy = 1; iy < cols - 1; iy++) {
      u[ix][iy] = 1.0;
    }
  }
}
/**************************************************************************
 * subroutine prtdat
 **************************************************************************/

void prtdat(int rows,int cols,double **u,char *fnam)
{
  int ix;
  int iy;
  FILE *fp;
  fp = fopen(fnam,"w");
  for (ix = 0; ix < rows; ix++) {
    for (iy = 0; iy < cols; iy++) {
      fprintf(fp,"%f ",u[ix][iy]);
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}
/*****************************************************************************
 *  subroutine inverse error propagation
 *****************************************************************************/

int **invert_propagation(int *x,int step,int *nPRC,int *ABFR_PRC_RANK)
{
  int x_lo = NX / numtasks * taskid;
  int x_hi = NX / numtasks * (taskid + 1) - 1;
  int l = x[0] - step > 0?x[0] - step : 0;
  int h = x[0] + step < NX?x[0] + step : NX - 1;
  if (x_lo > h || x_hi < l) {
     *ABFR_PRC_RANK = 0;
     *nPRC = 0;
    return ((void *)0);
  }
   else {
     *ABFR_PRC_RANK = 1;
  }
  int i;
  int j;
  int xstart;
  int xend;
  int ystart;
  int yend;
  xstart = (x_lo > l?x_lo : l);
  xend = (x_hi < h?x_hi : h);
  ystart = (x[1] - step > 0?x[1] - step : 0);
  yend = (x[1] + step < NY?x[1] + step : NY - 1);
   *nPRC = (xend - xstart + 1) * (yend - ystart + 1);
  int **PRCs = (int **)(calloc(sizeof(int *),( *nPRC)));
  for (i = 0; i <  *nPRC; i++) {
// 2-dimension
    PRCs[i] = ((int *)(calloc(sizeof(int ),2)));
  }
  int k = 0;
  int bx = NX / numtasks * taskid;
  for (i = xstart; i <= xend; i++) {
    for (j = ystart; j <= yend; j++) {
      PRCs[k][0] = i - bx + 1;
      PRCs[k][1] = j;
      k++;
    }
  }
  return PRCs;
}
/*****************************************************************************
 *  subroutine compute given elements
 *****************************************************************************/

void compute(int **PRCs,int nPRC,double **u)
{
  int i;
  int ix;
  int iy;
  for (i = 0; i < nPRC; i++) {
    ix = PRCs[i][0];
    iy = PRCs[i][1];
    ut[ix][iy] = u[ix][iy] + parms . cx * (u[ix + 1][iy] + u[ix - 1][iy] - 2.0 * u[ix][iy]) + parms . cy * (u[ix][iy + 1] + u[ix][iy - 1] - 2.0 * u[ix][iy]);
  }
  for (i = 0; i < nPRC; i++) {
    ix = PRCs[i][0];
    iy = PRCs[i][1];
    u[ix][iy] = ut[ix][iy];
  }
}

void abfr_recovery(double **u,GDS_gds_t gds_u,int ndims,size_t *count,size_t *ld,size_t *lo_index,size_t *hi_index,int t,int *error_index,int abfr_latency,int abfr_interval,double error_tol)
{
  int abfr_n = abfr_latency / abfr_interval;
  int abfr_MyRank;
  int *abfr_error;
  int abfr_i;
  int abfr_j;
  int abfr_k;
  int abfr_l;
  int abfr_step;
  int abfr_nPRC;
  int **abfr_PRCs;
  int abfr_PRC_Rank;
  int abfr_rankRaised;
  int abfr_EFLAG = 0;
  int abfr_EFLAG_SUM = 0;
  int abfr_ERANK = 0;
  GDS_size_t ver;
  double **abfr_buf;
  MPI_Comm_rank(MPI_COMM_WORLD,&abfr_MyRank);
  abfr_error = ((int *)(calloc(sizeof(int ),ndims)));
  abfr_buf = ((double **)(calloc(sizeof(double *),ndims)));
  for (abfr_i = 0; abfr_i < ndims; abfr_i += 1) 
    abfr_buf[abfr_i] = ((double *)(calloc(sizeof(double ),count[abfr_i])));
  GDS_version_dec(gds_u,abfr_n);
  GDS_get_version_number(gds_u,&ver);
  for (abfr_i = 0; abfr_i < abfr_n; ++abfr_i) {
    abfr_l = (abfr_n - abfr_i) * abfr_interval;
    abfr_step = t - abfr_l;
    abfr_PRCs = invert_propagation(error_index,abfr_l,&abfr_nPRC,&abfr_PRC_Rank);
    if (abfr_PRC_Rank == 1) {
      GDS_get(u,ld,lo_index,hi_index,gds_u);
      for (; abfr_step < t - abfr_l + interval; ++abfr_step) {
        for (abfr_j = 0; abfr_j < abfr_nPRC; ++abfr_j) {
          compute(abfr_PRCs,abfr_nPRC,u);
        }
      }
    }
    GDS_wait(gds_u);
    GDS_move_to_next(gds_u);
    if (abfr_PRC_Rank == 1) {
      GDS_get(abfr_buf,ld,lo_index,hi_index,gds_u);
      for (abfr_j = 0; abfr_j < abfr_nPRC; ++abfr_j) {
        double diff = u[abfr_PRCs[abfr_j][0]][abfr_PRCs[abfr_j][1]] - ut[abfr_PRCs[abfr_j][0]][abfr_PRCs[abfr_j][1]];
        diff = (diff > 0?diff : -diff);
        if (diff > error_tol) {
          for (abfr_k = 0; abfr_k < ndims; ++abfr_k) 
            abfr_error[abfr_k] = abfr_PRCs[abfr_j][abfr_k];
          break; 
        }
      }
    }
    MPI_Allreduce(&abfr_EFLAG,&abfr_EFLAG_SUM,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    if (abfr_EFLAG_SUM > 0) {
      if (abfr_EFLAG > 0) 
        abfr_rankRaised = abfr_MyRank;
      MPI_Allreduce(&abfr_rankRaised,&abfr_ERANK,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
      MPI_Bcast(abfr_error,ndims,MPI_INT,abfr_ERANK,MPI_COMM_WORLD);
      break; 
    }
  }
  abfr_l = (abfr_n - abfr_i) * abfr_interval;
  abfr_i = 0;
  if (abfr_PRC_Rank == 1) {
    for (abfr_step = t - abfr_l + 1; abfr_step < t; ++abfr_step) {
      abfr_PRCs = invert_propagation(abfr_error,abfr_i,&abfr_nPRC,&abfr_PRC_Rank);
      compute(abfr_PRCs,abfr_nPRC,u);
      ++abfr_i;
    }
  }
  GDS_move_to_newest(gds_u);
}
