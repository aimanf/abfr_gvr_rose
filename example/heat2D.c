/****************************************************************************
 * FILE: mpi_heat2D.c
 * DESCRIPTIONS:  
 *   HEAT2D Example - Parallelized C Version
 *   This example is based on a simplified two-dimensional heat 
 *   equation domain decomposition.  The initial temperature is computed to be 
 *   high in the middle of the domain and zero at the boundaries.  The 
 *   boundaries are held at zero throughout the simulation.  During the 
 *   time-stepping, an array containing two domains is used; these domains 
 *   alternate between old data and new data.
 *
 * AUTHOR: Aiman Fang - adpated from Blaise Barney and D. Turner. 
 * LAST REVISED: 11/25/17
 ****************************************************************************/
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NONE        -1                  /* indicates no neighbor */
#define DONE        4                  /* message tag */
#define MASTER      0                  /* taskid of first process */
#define LTAG        1                  /* message tag */
#define RTAG        2                  /* message tag */

int NX;
int NY;
int STEPS;
int taskid;
int numtasks;
int latency;
int interval;
double error_tol = 0.001;
int DetectedStep;
int AL; // actual latency
double  ** ut;                 /* shadow array for grid */

struct Parms { 
    double cx;
    double cy;
} parms = {0.1, 0.1};

int main(int argc, char *argv[])
{
    void inidat(), prtdat(), update();
    int **invert_propagation(int *x,int step,int *nPRC,int *ABFR_PRC_RANK);
    double  ** u;               /* array for grid */
    int rows, cols,             /* local rows, columns */
        dest, source,           /* to - from for message send-receive */
        left,right,             /* neighbor tasks */
        msgtype,                /* for message types */
        start,end,              /* misc */
        i,ix,iy,it;             /* loop variables */
    MPI_Status status[4];
    MPI_Request rqst[4];
    double tbegin, tend, telapsed;
    double tb_rcv, te_rcv;
    int error_detected = 0;
    
#pragma abfr_gvr_init argc argv
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&taskid);

    // parse parameters
    if(argc < 7) {
        if(taskid == MASTER) printf("Miss --NX, --NY\n");
        return 0;
    }
    for(i = 1; i < argc; i++) {
        if(!strcmp(argv[i],"--NX")) {
            NX = atoi(argv[++i]);
            if(taskid == MASTER) printf("NX=%d\n",NX);
        } 
        else if(!strcmp(argv[i],"--NY")) {
            NY = atoi(argv[++i]);
            if(taskid == MASTER) printf("NY=%d\n",NY);
        } 
        else if(!strcmp(argv[i],"--STEPS")) {
            STEPS = atoi(argv[++i]);
            if(taskid == MASTER) printf("STEPS=%d\n",STEPS);
        } 
        else if(!strcmp(argv[i],"--DetectedStep")) {
            DetectedStep = atoi(argv[++i]);
            if(taskid == MASTER) printf("DetectedStep=%d\n",DetectedStep);
        } 
        else if(!strcmp(argv[i],"--AL")) {
            AL = atoi(argv[++i]);
            if(taskid == MASTER) printf("AL=%d\n",AL);
        } 
        else if(!strcmp(argv[i],"--latency")) {
            latency = atoi(argv[++i]);
            if(taskid == MASTER) printf("latency=%d\n",latency);
        } 
        else if(!strcmp(argv[i],"--interval")) {
            interval = atoi(argv[++i]);
            if(taskid == MASTER) printf("interval=%d\n",interval);
        } 
    }

    int ndims = 2;
    size_t count[2] = {NX, NY};
#pragma abfr_init_version u ndims count double gds_u
    
    rows = NX / numtasks + 2; // 2 additional halo rows 
    cols = NY;

    u = (double **)calloc(sizeof(double *), rows);
    ut = (double **)calloc(sizeof(double *), rows);
    for(i = 0; i < rows; i++) {
        u[i] = (double *)calloc(sizeof(double), NY);
        ut[i] = (double *)calloc(sizeof(double), NY);
    }

    if(taskid == MASTER){
        printf ("Starting mpi_heat2D with %d worker tasks.\n", numtasks);

        /* Initialize grid */
        printf("Grid size: X= %d  Y= %d  Time steps= %d\n",NX,NY,STEPS);
        printf("Initializing grid and writing initial.dat file...\n");
        prtdat(rows, cols, u, "initial.dat");
    }   /* End of master code */

    // initial condition: high in the middle of the domain and zero at the
    // boundaries
    inidat(rows, cols, u);

    // Assign neighbors
    if(taskid == 0)
        left = NONE;
    else
        left = taskid - 1;
    if(taskid == numtasks - 1)
        right = NONE;
    else
        right = taskid + 1;

    /* Initialize everything - including the borders - to zero */
    for(ix = 0; ix < rows; ix++) { 
        for(iy = 0; iy < cols; iy++) {
            ut[ix][iy] = 0.0;
        }
    }

    /* Begin doing STEPS iterations.  Must communicate border rows with */
    /* neighbors.  If I have the first or last grid row, then I only need */
    /* to communicate with one neighbor  */
    MPI_Barrier(MPI_COMM_WORLD);
    if(taskid == MASTER) printf("Begin time steps...\n");

    for (it = 1; it <= STEPS; it++)
    {
        tbegin = MPI_Wtime();
        // exchange left ghost row
        if(left != NONE) {
            MPI_Isend(&u[1][0], NY, MPI_DOUBLE, left, 
                    RTAG, MPI_COMM_WORLD, &rqst[0]);
            MPI_Irecv(&u[0][0], NY, MPI_DOUBLE, left,
                    LTAG, MPI_COMM_WORLD, &rqst[1]);
        }
        // exchange right ghost row
        if(right != NONE) {
            MPI_Isend(&u[rows-2][0], NY, MPI_DOUBLE, right,
                    LTAG, MPI_COMM_WORLD, &rqst[2]);
            MPI_Irecv(&u[rows-1][0], NY, MPI_DOUBLE, right,
                    RTAG, MPI_COMM_WORLD, &rqst[3]);
        }
        if(left == NONE) {
            MPI_Waitall(2, &rqst[2], &status[2]);
        } 
        else if(right == NONE) {
            MPI_Waitall(2, &rqst[0], &status[0]);
        }
        else {
            MPI_Waitall(4, rqst, status);
        }

        /* Now call update to update the value of grid points */
        update(rows, cols, u, ut);

        if(error_detected == 0 && it == DetectedStep) {
            MPI_Barrier(MPI_COMM_WORLD);
            if(taskid == 0) printf("An error is raised\n");
            int ndims = 2;
            size_t count[2] = {NX, NY};
            size_t ld[1] = {NY};
            size_t lo_index[2] = {(NX/numtasks)*taskid,0};
            size_t hi_index[2] = {(NX/numtasks)*(taskid+1)-1,NY-1};
            int error_index[2] = {NX/2, NY/2};
            tb_rcv = MPI_Wtime();
            
#pragma abfr_recover u gds_u error_index it latency interval error_tol invert_propagation compute
            
            te_rcv = MPI_Wtime();
            error_detected = 1;
            MPI_Barrier(MPI_COMM_WORLD);
        }

        size_t ld[1] = {NY};
        size_t lo_index[2] = {(NX/numtasks)*taskid,0};
        size_t hi_index[2] = {(NX/numtasks)*(taskid+1)-1,NY-1};
        if(it % interval == 0) {
#pragma abfr_versioning u ld lo_index hi_index gds_u
            if(taskid == MASTER) printf("versioning at step %d\n", it);
        }


        tend = MPI_Wtime();
        if(taskid == MASTER) printf("step %d takes %f seconds\n", it, tend-tbegin);
        telapsed += tend - tbegin;
    }
    if(taskid == MASTER) {
        printf("Writing final.dat file.\n");
        prtdat(rows, cols, u, "final.dat");
        printf("Simulation time = %f\n",telapsed);
    }

#pragma abfr_gvr_finalize
    MPI_Finalize();
}


/**************************************************************************
 *  subroutine update
 ****************************************************************************/
void update(int rows, int cols, double **u, double **ut)
{
    int ix, iy;
    for(ix = 1; ix < rows - 1; ix++) {
        for(iy = 0; iy < cols; iy++) {
            ut[ix][iy] = u[ix][iy] +  
                parms.cx * (u[ix+1][iy] + u[ix-1][iy] - 2.0 * u[ix][iy]) +
                parms.cy * (u[ix][iy+1] + u[ix][iy-1] - 2.0 * u[ix][iy]);
        }
    }

    for(ix = 1; ix < rows - 1; ix++) {
        for(iy = 0; iy < cols; iy++) {
            u[ix][iy] = ut[ix][iy];
        }
    }
}

/*****************************************************************************
 *  subroutine inidat
 *****************************************************************************/
void inidat(int rows, int cols, double **u) {
    int ix, iy;
    int nrows = rows - 2; // actual num of rows assigned to each task excluding 2 halo rows
    for(ix = 1; ix <= nrows; ix++) {
        for(iy = 1; iy < cols-1; iy++) {
            u[ix][iy] = 1.0;
        }
    }
}

/**************************************************************************
 * subroutine prtdat
 **************************************************************************/
void prtdat(int rows, int cols, double **u, char *fnam) {
    int ix, iy;
    FILE *fp;

    fp = fopen(fnam, "w");
    for(ix = 0; ix < rows; ix++) {
        for(iy = 0; iy < cols; iy++) {
            fprintf(fp, "%f ", u[ix][iy]);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
}

/*****************************************************************************
 *  subroutine inverse error propagation
 *****************************************************************************/
int **invert_propagation(int *x,int step,int *nPRC,int *ABFR_PRC_RANK)
{
    int x_lo = (NX / numtasks) * taskid;
    int x_hi = (NX / numtasks) * (taskid + 1) - 1;

    int l = x[0]-step > 0 ? x[0]-step : 0;
    int h = x[0]+step < NX ? x[0]+step : NX-1;

    if(x_lo > h || x_hi < l) {
        *ABFR_PRC_RANK = 0;
        *nPRC = 0;
        return NULL;
    }
    else {
        *ABFR_PRC_RANK = 1;
    }

    int i, j, xstart, xend, ystart, yend;
    xstart = x_lo > l ? x_lo : l;
    xend = x_hi < h ? x_hi : h;
    ystart = x[1]-step > 0 ? x[1]-step : 0;
    yend = x[1]+step < NY ? x[1]+step : NY-1;

    *nPRC = (xend - xstart + 1) * (yend - ystart + 1);
    int ** PRCs = (int **)calloc(sizeof(int *), *nPRC);
    for(i = 0; i < *nPRC; i++) {
        PRCs[i] = (int *)calloc(sizeof(int), 2); // 2-dimension
    }
    int k = 0;
    int bx = (NX/numtasks)*taskid;
    for(i = xstart; i <= xend; i++) {
        for(j = ystart; j <= yend; j++) {
            PRCs[k][0] = i-bx+1;
            PRCs[k][1] = j;
            k++;
        }
    }

    return PRCs;
}

/*****************************************************************************
 *  subroutine compute given elements
 *****************************************************************************/
void compute(int ** PRCs, int nPRC, double ** u)
{
    int i, ix, iy;
    for(i = 0; i < nPRC; i++) {
        ix = PRCs[i][0];
        iy = PRCs[i][1]; 
        ut[ix][iy] = u[ix][iy] +  
            parms.cx * (u[ix+1][iy] + u[ix-1][iy] - 2.0 * u[ix][iy]) +
            parms.cy * (u[ix][iy+1] + u[ix][iy-1] - 2.0 * u[ix][iy]);
        
    }
    
    for(i = 0; i < nPRC; i++) {
        ix = PRCs[i][0];
        iy = PRCs[i][1]; 
        u[ix][iy] = ut[ix][iy];
    }
}
